package com.pashonokk.ecommerce.commerceApi;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.defaultconfig.ApiRootBuilder;
import com.pashonokk.ecommerce.util.CtpProperties;
import io.vrap.rmf.base.client.oauth2.ClientCredentials;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class Client {

    private final CtpProperties ctpProperties;
    private final ServiceRegion serviceRegion;

    public ProjectApiRoot createApiClient() {
        return ApiRootBuilder.of()
                             .defaultClient(ClientCredentials.of()
                                                             .withClientId(ctpProperties.getClientId())
                                                             .withClientSecret(ctpProperties.getClientSecret())
                                                             .build(),
                                            serviceRegion)
                             .build(ctpProperties.getProjectKey());
    }

}