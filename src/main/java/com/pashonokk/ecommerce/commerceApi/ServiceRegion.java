package com.pashonokk.ecommerce.commerceApi;

import com.pashonokk.ecommerce.util.CtpProperties;
import io.vrap.rmf.base.client.ServiceRegionConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ServiceRegion
        implements ServiceRegionConfig {

    private final CtpProperties ctpProperties;

    @Override
    public String getApiUrl() {
        return ctpProperties.getApiUrl();
    }

    @Override
    public String getAuthUrl() {
        return ctpProperties.getAuthUrl();
    }

}
