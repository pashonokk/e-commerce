package com.pashonokk.ecommerce.annotation;

import com.pashonokk.ecommerce.util.CustomOffsetDateTimeValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = CustomOffsetDateTimeValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidOffsetDateTime {
    String message() default "Invalid OffsetDateTime format, try using such format yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}