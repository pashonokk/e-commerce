package com.pashonokk.ecommerce.mapper;

import com.pashonokk.ecommerce.dto.UserDto;
import com.pashonokk.ecommerce.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto toDto(User user);
}
