package com.pashonokk.ecommerce.mapper;

import com.pashonokk.ecommerce.dto.UserSavingDto;
import com.pashonokk.ecommerce.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserSavingMapper {
    User toEntity(UserSavingDto userDto);

    UserSavingDto toDto(User user);
}
