package com.pashonokk.ecommerce.mapper;

import com.pashonokk.ecommerce.dto.PermissionSavingDto;
import com.pashonokk.ecommerce.entity.Permission;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PermissionSavingMapper {
    Permission toEntity(PermissionSavingDto permissionSavingDto);

    PermissionSavingDto toDto(Permission permission);
}
