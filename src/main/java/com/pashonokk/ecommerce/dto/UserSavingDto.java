package com.pashonokk.ecommerce.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSavingDto {
    @Email(message = "You entered wrong email")
    private String email;
    @NotBlank(message = "Password can`t be empty or null")
    @Size(min = 10, message = "Password must have more than 10 characters")
    private String password;
    @NotBlank(message = "FirstName can`t be empty")
    private String firstName;
    @NotBlank(message = "LastName can`t be empty")
    private String lastName;
    @NotBlank(message = "Username can`t be empty")
    private String username;
}
