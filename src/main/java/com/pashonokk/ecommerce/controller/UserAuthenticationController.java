package com.pashonokk.ecommerce.controller;

import com.pashonokk.ecommerce.dto.UserSavingDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserAuthenticationController {

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("user", new UserSavingDto());
        return "login";
    }
}
