package com.pashonokk.ecommerce.controller;

import com.pashonokk.ecommerce.dto.UserSavingDto;
import com.pashonokk.ecommerce.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
@RequiredArgsConstructor
public class UserRegisterController {
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(UserRegisterController.class);

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new UserSavingDto());
        return "registration";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute() @Valid UserSavingDto user, Errors errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors().forEach(er -> logger.error(er.getDefaultMessage()));
            return "registration";
        }
        userService.saveRegisteredUser(user);
        return "redirect:/login";
    }


}
