package com.pashonokk.ecommerce.controller;

import com.commercetools.api.models.cart.Cart;
import com.pashonokk.ecommerce.dto.ProductDto;
import com.pashonokk.ecommerce.service.CartService;
import com.pashonokk.ecommerce.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/carts")
public class CartController {
    private final CartService cartService;
    private final ProductService productService;

    @ModelAttribute("products")
    public List<ProductDto> addProductsToModel() {
        return productService.getAllProducts();
    }

    @GetMapping("/{productId}")
    public String addProductToCart(@PathVariable String productId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Cart cart = cartService.addProductToCart(productId, authentication.getName());
        model.addAttribute("cart", cart);
        return "products-catalog";
    }

}
