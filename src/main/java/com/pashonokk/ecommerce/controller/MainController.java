package com.pashonokk.ecommerce.controller;

import com.pashonokk.ecommerce.dto.ProductDto;
import com.pashonokk.ecommerce.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/home")
public class MainController {
    private final ProductService productService;

    @GetMapping
    public String home(Model model) {
        List<ProductDto> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products-catalog";
    }

}
