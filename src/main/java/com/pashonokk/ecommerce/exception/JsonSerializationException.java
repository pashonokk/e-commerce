package com.pashonokk.ecommerce.exception;

import org.springframework.http.HttpStatus;

public class JsonSerializationException extends GenericDisplayableException {
    public JsonSerializationException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
