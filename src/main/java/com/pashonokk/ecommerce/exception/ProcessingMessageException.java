package com.pashonokk.ecommerce.exception;

import org.springframework.http.HttpStatus;

public class ProcessingMessageException extends GenericDisplayableException {
    public ProcessingMessageException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
