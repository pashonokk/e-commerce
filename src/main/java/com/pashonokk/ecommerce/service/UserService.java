package com.pashonokk.ecommerce.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pashonokk.ecommerce.dto.UserSavingDto;
import com.pashonokk.ecommerce.entity.Role;
import com.pashonokk.ecommerce.entity.User;
import com.pashonokk.ecommerce.exception.JsonSerializationException;
import com.pashonokk.ecommerce.exception.UserExistsException;
import com.pashonokk.ecommerce.mapper.UserSavingMapper;
import com.pashonokk.ecommerce.repository.RoleRepository;
import com.pashonokk.ecommerce.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Transactional
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserSavingMapper userSavingMapper;
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    private static final String USER_EXISTS_ERROR_MESSAGE = "User with email %s already exists";


    public void saveRegisteredUser(UserSavingDto userDto) {
        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new UserExistsException(String.format(USER_EXISTS_ERROR_MESSAGE, userDto.getEmail()));
        }
        User user = userSavingMapper.toEntity(userDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role roleUser = roleRepository.findRoleByName("ROLE_CUSTOMER");
        user.setRole(roleUser);
        User savedUser = userRepository.save(user);
        notifyToSaveUserInCommerceTools(savedUser);
    }

    private void notifyToSaveUserInCommerceTools(User savedUser) {
        String savedUserJson;
        try {
            savedUserJson = objectMapper.writeValueAsString(savedUser);
        } catch (JsonProcessingException e) {
            throw new JsonSerializationException("failed to serialize " + savedUser);
        }
        rabbitTemplate.setExchange("fanoutExchange");
        rabbitTemplate.convertAndSend(savedUserJson);
    }
}
