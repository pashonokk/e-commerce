package com.pashonokk.ecommerce.service;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.cart.Cart;
import com.commercetools.api.models.cart.CartDraft;
import com.commercetools.api.models.cart.CartUpdate;
import com.commercetools.api.models.customer.CustomerPagedQueryResponse;
import com.pashonokk.ecommerce.commerceApi.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartService {
    private final Client commerceApiClient;

    public Cart addProductToCart(String productId, String email) {
        ProjectApiRoot apiRoot = commerceApiClient.createApiClient();
        String customerId = getCustomerIdByEmail(email, apiRoot);
        Cart customerCart = getCustomerCart(customerId, apiRoot, email);

        CartUpdate cartUpdate = CartUpdate.builder()
                .version(1L)
                .plusActions(action -> action.addLineItemBuilder()
                        .productId(productId)
                        .quantity(1L)
                        .variantId(1L)
                )
                .build();

        apiRoot
                .carts()
                .withId(customerCart.getId())
                .post(cartUpdate)
                .executeBlocking()
                .getBody();
        return customerCart;
    }

    private Cart getCustomerCart(String customerId, ProjectApiRoot apiRoot, String email) {
        Cart customerCart;
        try {
            customerCart = apiRoot
                    .carts()
                    .withCustomerId(customerId)
                    .get()
                    .executeBlocking()
                    .getBody();
        } catch (Exception e) {
            CartDraft cartDraft = CartDraft.builder()
                    .customerId(customerId)
                    .customerEmail(email)
                    .currency("USD")
                    .build();
            return apiRoot
                    .carts()
                    .post(cartDraft)
                    .executeBlocking()
                    .getBody();
        }
        return customerCart;
    }

    private String getCustomerIdByEmail(String email, ProjectApiRoot apiRoot) {
        CustomerPagedQueryResponse customerToFind = apiRoot
                .customers()
                .get()
                .withWhere("email = :customerEmail", "customerEmail", email)
                .executeBlocking()
                .getBody();

        return customerToFind.getResults().get(0).getId();
    }
}
