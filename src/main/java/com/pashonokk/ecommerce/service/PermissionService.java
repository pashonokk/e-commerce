package com.pashonokk.ecommerce.service;

import com.pashonokk.ecommerce.dto.PermissionDto;
import com.pashonokk.ecommerce.dto.PermissionSavingDto;
import com.pashonokk.ecommerce.entity.Permission;
import com.pashonokk.ecommerce.entity.Role;
import com.pashonokk.ecommerce.mapper.PermissionMapper;
import com.pashonokk.ecommerce.mapper.PermissionSavingMapper;
import com.pashonokk.ecommerce.repository.PermissionRepository;
import com.pashonokk.ecommerce.repository.RoleRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PermissionService {
    private final PermissionRepository permissionRepository;
    private final RoleRepository roleRepository;
    private final PermissionMapper permissionMapper;
    private final PermissionSavingMapper permissionSavingMapper;
    private static final String PERMISSION_ERROR_MESSAGE = "Permission with id %s doesn't exist";


    @Transactional(readOnly = true)
    public PermissionDto getPermissionById(Long id) {
        Permission permission = permissionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(PERMISSION_ERROR_MESSAGE, id)));
        return permissionMapper.toDto(permission);
    }

    @Transactional
    public PermissionDto addPermission(PermissionSavingDto permissionSavingDto) {
        Permission permission = permissionSavingMapper.toEntity(permissionSavingDto);
        List<Role> rolesById = roleRepository.findAllByIdAndPermissions(permissionSavingDto.getRoleIds());
        permission.addRoles(rolesById);
        permission.setIsDeleted(false);
        Permission savedPermission = permissionRepository.save(permission);
        return permissionMapper.toDto(savedPermission);
    }

    @Transactional
    public void deletePermission(Long id) {
        Permission permission = permissionRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(PERMISSION_ERROR_MESSAGE, id)));
        permission.setIsDeleted(true);
    }
}
