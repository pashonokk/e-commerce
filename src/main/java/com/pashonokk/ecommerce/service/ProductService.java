package com.pashonokk.ecommerce.service;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.common.Image;
import com.commercetools.api.models.product.Product;
import com.commercetools.api.models.product.ProductData;
import com.pashonokk.ecommerce.commerceApi.Client;
import com.pashonokk.ecommerce.dto.ProductDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final Client commerceApiClient;

    public List<ProductDto> getAllProducts() {
        ProjectApiRoot apiRoot = commerceApiClient.createApiClient();
        return apiRoot
                .products()
                .get()
                .executeBlocking()
                .getBody()
                .getResults()
                .stream()
                .map(this::mapProductsToProductDto).toList();
    }

    public ProductDto mapProductsToProductDto(Product product) {
        ProductData current = product.getMasterData().getCurrent();
        String productId = product.getId();
//        String description = current.getDescription().find(Locale.ENGLISH).orElse("default description");
        String name = current.getName().get("en-US");
        String imageUrl = current.getMasterVariant().getImages()
                .stream()
                .map(Image::getUrl)
                .findFirst()
                .orElse("nothing");
        return new ProductDto(productId, name, "desc", imageUrl);
    }

}
