package com.pashonokk.ecommerce.listener;

import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.customer.CustomerDraft;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pashonokk.ecommerce.commerceApi.Client;
import com.pashonokk.ecommerce.entity.User;
import com.pashonokk.ecommerce.exception.ProcessingMessageException;
import com.rabbitmq.client.Channel;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class UserRegistrationListener {
    private final Client commerceApiClient;
    private final ObjectMapper objectMapper;

    @RabbitListener(queues = "usersToSaveInCT")
    public void saveUserToCommerceTools(String savedUserJson, Message message, Channel channel) throws JsonProcessingException {
        User savedUser = objectMapper.readValue(savedUserJson, User.class);
        CustomerDraft newCustomerDetails = constructCustomerDraft(savedUser);
        try (ProjectApiRoot apiRoot = commerceApiClient.createApiClient()) {
            apiRoot
                    .customers()
                    .post(newCustomerDetails)
                    .executeBlocking();
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            try {
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            } catch (IOException ex) {
                throw new ProcessingMessageException("Message cannot be marked as unread");
            }
        }
    }

    private static CustomerDraft constructCustomerDraft(User savedUser) {
        return CustomerDraft
                .builder()
                .email(savedUser.getEmail())
                .password(savedUser.getPassword())
                .firstName(savedUser.getFirstName())
                .lastName(savedUser.getLastName())
                .build();
    }
}
