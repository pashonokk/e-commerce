package com.pashonokk.ecommerce.endpoint.dto;


import com.pashonokk.ecommerce.endpoint.BaseResponse;
import com.pashonokk.ecommerce.endpoint.Status;
import lombok.Getter;

import java.util.List;
@Getter
public class ValidationFailedResponse extends BaseResponse {
    private final List<String> errorMessages;
    public ValidationFailedResponse(String message, List<String> errorMessages) {
        super(Status.FAIL, message);
        this.errorMessages = errorMessages;
    }
}
