package com.pashonokk.ecommerce.endpoint.dto;

import com.pashonokk.ecommerce.endpoint.BaseResponse;
import com.pashonokk.ecommerce.endpoint.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FailureResponse extends BaseResponse {
    public FailureResponse(String message) {
        super(Status.FAIL, message);
    }

}