CREATE TABLE IF NOT EXISTS permission
(
    id         BIGSERIAL PRIMARY KEY,
    name       VARCHAR(100) NOT NULL,
    is_deleted BOOLEAN,
    UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS role_permission
(
    role_id       BIGINT,
    permission_id BIGINT,
    FOREIGN KEY (role_id) REFERENCES role (id),
    FOREIGN KEY (permission_id) REFERENCES permission (id),
    PRIMARY KEY (role_id, permission_id)
);